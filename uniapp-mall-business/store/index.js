import cache from '@/common/js/utils/cache.js'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

/**
 * context : 上下文 ---> 可以获取state中的参数
 * 如：context.state.token
 */
const carList = cache.get('carList') || [];
const userInfo = cache.get('userInfo') || '';

const store = new Vuex.Store({
	state: {
	  token: 'sss',
	  userInfo: userInfo,
	  carList: carList,
	},
	mutations: {
		// 登录成功，把userinfo存到仓库
	  getUserInfo(state,params){
		  state.userInfo = params;
		  uni.setStorageSync('userInfo',params);
		  // 删除提货人信息
		  cache.remove('myAdr')
	  },
	  // 退出登录，把userinfo移出仓库
	  removeUserInfo(state){
		  state.userInfo = '';
		  uni.removeStorage({key:'userInfo'});
		  // 清空仓库里购物车信息
		  state.carList = [];
		  cache.remove('carList')
	  },
	},
	actions: {
		getCity(context){
			
		}
	},
	getters:{

	}
});

export default store