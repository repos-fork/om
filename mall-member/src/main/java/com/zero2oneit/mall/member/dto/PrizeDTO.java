package com.zero2oneit.mall.member.dto;

import lombok.Data;

/**
 * Description: 抽奖数据传输对象
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021/5/27
 */
@Data
public class PrizeDTO {

    private String memberId;

    private String commission;

    private String nickName;

}
